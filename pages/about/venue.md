---
name: The Conference Venue
---

# Venue

DebConf24 will be held at [Pukyong National University](https://www.openstreetmap.org/#map=18/35.13150/129.10370).

Pukyong National University (PKNU) is a national university situated in the heart of urban Busan. Spanning 36 hectares, the campus includes 44 accommodation apartments, administrative buildings, maintenance facilities, workshop sheds, a sports complex, and recreational spaces like cafes and a modern canteen.

## Address

45 Yongso-ro
Nam-gu
Busan
48513
Republic of Korea

## Rooms

* Engineering 1 building
	* Talk room
	* Talk room for BoFs
	* 1 Silent Hacklab
	* 2 General Hacklabs
	* Front Desk room
	* Video Team room
* Mirae building
	* Somin hall - Talk room
	* 2 Ad Hoc rooms
	* Dining space
	* Social space
* Hyangpa building
	* 4 Ad Hoc rooms

## Room floor maps

The PDF printable version of the room floor maps can be [found here](https://salsa.debian.org/debconf-team/public/data/dc24/-/raw/main/venue/floorplans/PKNU-Venue-floor-map.pdf).


# Getting to Busan

## Purchasing your plane ticket

### Compare tickets for two destinations, PUS vs ICN.

* Gimhae International Airport (PUS) is the nearby airport. But when there's no convenient and reasonable priced ticket to PUS (especially when you are from Europe or Americas), please try to find a ticket to Incheon International Airport (ICN) nearby Seoul. ICN is much bigger and have more flights than PUS, so you will probably find a better ticket.
	* IATA code PUS means Pusan (former romanization of Busan), but official name is Gimhae International Airport. Please note and be careful not to get confused.
* When you do a comparison, consider the cost and the time from ICN to Busan. If you use the high speed train (KTX) and the airport express train (AREX), the round trip will cost about 120,000 KRW in total. And it took 4 hours to move from ICN to Busan.
* When you are going to arrive at ICN, be careful about arriving and departing time. There is no overnight train in the country; you cannot take train between 23:30 to 05:30.
	* There is an airport limousine bus couch, but it will take 6 hours and the fare is similar to the high speed train. But for late night arriving or early morning departing, it can be a choice.
	* Another option is, of course, to take time to stay (and take a look around) in a closer location before or after DebConf.

### Ticket with ICN-PUS connection? Again, compare PUS vs ICN.

* You may find a ticket including ICN-PUS connection flight. Before buying it, please compare it and the one arriving ICN without the ICN-PUS connection. Adding ICN-PUS connection is sometimes very expensive like additional 500 USD or needs too much hours to wait at ICN, when taking train to Busan is a better choice.
* The background is,
	* This ICN-PUS flight can be purchased only as a connection of another ICN arriving or departing flight. And the current only operator of the ICN-PUS connection flights is Korean Air. So only Korean Air or SkyTeam codeshare flights can use the ICN-PUS connection.
	* When you are arriving ICN by Korean Air, mostly it doesn't add much cost and you won't wait much. In this case, it's reasonable to buy the ticket with the connection.
	* But for other SkyTeam codeshare flights, adding this ICN-PUS connection is usually very expensive and/or needs too much time for waiting.

## ICN to Busan

[Related Wiki: TouristGuide](https://wiki.debian.org/DebConf/24/TouristGuide).

Sometimes, the arrival/departure point is GMP; Gimpo International Airport. You can access to AREX; Airport Railroad, vise-versa.

### Using AREX for transfer to Seoul Station to board the KTX

<div class="alert alert-info">
Please check AREX's operating hour, before ride.
</div>

**Mobile Data Warning** AREX Website's user guide: [LINK](https://www.airportrailroad.com/co/steUsfGud.do#staMovingLine) Image Included.

After department, board the AREX to Seoul station, board the KTX for from Seoul station to Busan station.

* AREX Lines divided 2 Lines. Orange Color(#F97600) Direct Line and Ocean Color(#0090D2) All stop Line. You cannot transfer between Direct Line and All stop Line at midway stations.
 
* AREX ICN Terminal 1/2 station offers Express Train and All stop Train, AREX GMP station offers All stop Train only, but offers many Seoul Metropolitan Metro Lines.

#### ICN Terminal 1/2

**Towards Seoul station:**

* Arrival floor at ICN: Terminal 1-1F, Terminal 2-1F.
 
* ICN Terminal 1-To the Transportation Center: Go down to the B1. Note: The passenger terminal guidance signs 'AREX' is marked in yellow letters for following up.
 
* AREX ICN Terminal 1 station-B1: Ticket office or Vending machine.

* AREX ICN Terminal 1 station-B4: Ride Direct Train to Seoul station.

* ICN Terminal 2-To the Transportation Center: Go down to the B1.

* AREX ICN Terminal 2 station-B1: Ticket office or Vending machine. If you go to ICN Terminal 1, using '''All stop Train''' instead of Direct Train.

* AREX ICN Terminal 2 station-B3: Ride Direct Train to Seoul station.

#### GMP

Note: Curfew is 23:00 ~ 06:00(+1Day).

<div class="alert alert-info">
For International to Domestric Transfer, You must FIND YOUR LUGGAGE and move to GMP Domestic Terminal and then you have to CHECK-IN YOUR LUGGAGE AGAIN.
</div>

You can transfer to PUS. Please check your flight career's minimum transfer times, before planning and ticketing.

**Towards Seoul station:**

* Arrival floor at International Terminal-1F: For go to the Domestic Terminal, using underpass or go to the Gate 1 and ride Airport Shuttle Bus (Bus Stop No. 5).

* Domestic Terminal-1F: Go down To the B1.
 
* AREX GMP station-B1: The ticketing method is similar to the Seoul Metropolitan Metro. Recommend using a public transportation card, e.g. T-Money, Cashbee, etc. You can also use this Busan.

* AREX GMP station-B3: Ride All stop Train to Seoul station.

#### Seoul station
 
* Seoul station B7-AREX Platform: When you arrive at Seoul Station by AREX, it is your current location.
 
* Seoul station B3-AREX All stop Train Ticket Office: If you need to going back AREX GMP station. Or transfer to Seoul Metropolitan Metro Line 1 Blue Color(#0052A4), Line 4 Sky Color(#00A423) for into the City.
 
* Seoul station B2-AREX Express Train Ticket Office, City Airport Terminal (도심공항터미널)

* AREX Seoul Station-2F(or Korail Seoul station-1F): Please refer paragraph **KTX**.

### KTX

Korean Train eXpress, operated by Korail.

* Note: Inquery Korail Customer Center for English language: 1599-7777. Operation hours-08:00 ~ 20:00 / 7 Days.

#### Ticket Reservation

<div class="alert alert-info">
Overseas credit card CANNOT be used on stations' on-site vending machines. If you need to use veding machines, some machines support cash payment.
</div>

* It is possible to search the train timetable and purchase tickets from 1 month before the date of travel.

* After the train has departed, you should request a ticket refund at the station ticket counter.

* A refund fee is imposed based on the time of the refund. (See Korail pages' Refund Fee section)

* Tickets are valid until the train arrives at the destination. (You cannot refund the ticket after the train has arrived at the destination.)

* Website: [Korail-PC](https://www.letskorail.com/ebizbf/EbizBfTicketSearch.do), [Korail-Mobile](https://www.letskorail.com/ebizbf/EbizBfMainM.do).
	* You should make payment immediately after ticket reservation.
	* Your reservation is confirmed when payment is complete and the ticket is issued.
	* You cannot reserve tickets for a train departing in 20 minutes or less from the time of reservation on Website.
	* As you purshase a ticket, a confirmation of the reservation will be sent to the e-mail you wrote. You can check your reserved ticket via link in the e-mail or on the website.

* Mobile App: KorailTalk. [Google Play](https://play.google.com/store/apps/details?id=com.korail.talk&hl=en), [App Store](https://apps.apple.com/us/app/%EC%BD%94%EB%A0%88%EC%9D%BC%ED%86%A14/id1000558562)
	* cf. many feature of the foreign language version are omitted, but it provides a very simple interface.
	* For oversease credit card, please use the foreign language version of the KoreailTalk App. Korean version App may not support it.
